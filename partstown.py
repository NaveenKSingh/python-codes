import cloudscraper
from bs4 import BeautifulSoup
import pandas as pd
import math
import traceback,os

scraper = cloudscraper.create_scraper(delay=5,   browser={'custom': 'ScraperBot/1.0',})
# url = "https://www.partstown.com/hoshizaki/parts?q=fan+blade"
# url = "https://www.partstown.com/hoshizaki/parts?q=curtain&page=0"
# req= scraper.get(url)
# print(req)
# soup = BeautifulSoup(req.content, "html.parser")
# #​soup = BeautifulSoup(req.content, "html.parser")
# txt=soup.find_all('div',class_="details")
# print(txt)
# print(len(txt))

# total_part = int(soup.find("span",class_ = "product-tabs-item__count").get_text().strip("(").strip(")"))
# print(total_part)
# pages = 0
# if total_part>24:
#     pages = math.ceil(total_part/24)
#     print(pages)
# else:
#     pages = 1


def partstown(url,j):
    print("@@@@@@@@@@",url)
    result = scraper.get(url)
    soup = BeautifulSoup(result.text, "html.parser")
    item_no = "N/A"
    manufacturer = "N/A"
    mpn_no = "N/A"
    # try:
    #     product_name = soup.find("h1").get_text().strip()
    #     if product_name == "www.partstown.com":
    #         product_name = "N/A"
    #     quantity = 1
    #     # print(product_name)
    # except:
    #     product_name = "N/A"
    #     quantity = "N/A"
    #     # print(product_name)
    try:
        image_url = soup.find("img", itemtype="image")["src"]
        # print(image_url)
    except:
        image_url = None
        # print(image_url)
    try:
        price = (
            soup.find("div", class_="product__cell price-vat price-vat__mobile-only js-product-listPrice")
            .get_text()
            .strip()
        )
        # print(price)
    except:
        price = "N/A"
        # print(price)
    try:
        unit = soup.find(
            "div", class_="partspin js-partspin product-container js-multi-product-item"
        )["data-units"]
        # print(unit)
    except:
        unit = "N/A"
        # print(unit)
    try:
        description = (
            soup.find(
                "div", class_="product-description__content js-clamp clamp-content"
            )
            .get_text()
            .strip("\n")
            .replace("\n\n", "\n")
        )
        # print(description)
    except:
        description = "N/A"
        # print(description)
    try:
        spc_name = soup.find_all("div", class_="spec-list__label")
        spc_data = soup.find_all("div", class_="spec-list__value")
        # spc_data = soup.find_all("ul", class_="spec-list")
        print("--------------------------------------------------------------------------",spc_name,spc_data,"--------------------------------------------------------------------------------------")
        specifications = {}
        # print("\n\n000000000000000000",spc_name.get_text().strip())
        for i in range(0,len(spc_name)):
            print("i: ",spc_name[i])
            print(spc_name[i].get_text())
            print(spc_data[i].get_text().strip("").strip("\n").encode("ascii", "ignore").decode("utf-8", "ignore").replace("\n", ""))
            specifications[spc_name[i].get_text()] = (
                spc_data[i]
                .get_text()
                .strip("")
                .strip("\n")
                .encode("ascii", "ignore")
                .decode("utf-8", "ignore")
                .replace("\n", "")
            )
            print("#########################################################",specifications)
            if "quantity" in spc_name[i].get_text.lower():
                quantity=spc_data[i].get_text().strip("").strip("\n").encode("ascii", "ignore").decode("utf-8", "ignore").replace("\n", "")
        
    except:
        
        specifications = {}
    try:
        sub_category = (
            soup.find("ol", class_="breadcrumb js-breadcrumb")
            .find("li", class_="active")
            .get_text()
            .strip()
        )
    except:
        sub_category = "N/A"
    try:
        numbers = soup.find_all("div", {"class": "product__row"})
        for number in numbers:
            if number.text is not None:
                try:
                    if (
                        "manufacturer" in number.text.lower()
                        and "#" not in number.text.lower()
                    ):
                        manufacturer = number.text
                        manufacturer = manufacturer.replace("\n", "")
                        manufacturer = manufacturer.split(":")[1]
                except:
                    manufacturer = "N/A"
                try:
                    if (
                        "manufacturer" in number.text.lower()
                        and "#" in number.text.lower()
                    ):
                        mpn_no = number.text
                        mpn_no = mpn_no.replace("\n", "")
                        mpn_no = mpn_no.split(":")[1]
                except:
                    mpn_no = "N/A"
                try:
                    if "parts" in number.text.lower() and "#" in number.text.lower():
                        item_no = number.text
                        item_no = item_no.replace("\n", "")
                        item_no = item_no.split(":")[1]
                except:
                    item_no = "N/A"
    except Exception as e:
        item_no = item_no
    try:
        cat_data = soup.find_all("span", itemprop="name")
        # print(cat_data)
        # category = cat_data[1].get_text().strip()
        category = j
        # print(category)
    except:
        category = "N/A"
        # print(category)

    

    try:
        url = f"https://www.partstown.com/hoshizaki/hos{mpn_no}#id=spec"
        part_url = url
        print("#############",url)
        req= scraper.get(url)
        soup = BeautifulSoup(req.text, "html.parser")
        spec = soup.find("ul", class_="spec-list")
        print(spec)
        label = spec.find_all("div", class_="spec-list__label")
        lab_val = spec.find_all("div", class_ = "spec-list__value")
        specification = {}
        for dt in range(len(label)):
            specification[label[dt].text.replace("\xa0", "").replace("\n", "")] = lab_val[dt].text.replace("\xa0", "").replace("\n", "")
    except:
        specification="N/A"
        pass

    # try:
    #     with open("prettified.txt","w") as htmlp:
    #         htmlp.write(str(soup))
    #         # htmlp.save()
    #         htmlp.close()
    #     # spc_name = soup.find_all("div", class_="spec-list__label")
    #     fits_data = soup.find_all("div", class_="tab-content__data")
    #     input(("1 ",fits_data.prettify()))
    #     fits_data = fits_data[0].find("div",class_="tab-content__items")
    #     print("2 ",fits_data.prettify())
    #     fits_data = fits_data.find_all("div") #,class_="data-sheet data-sheet--vue")
    #     print("3 ",fits_data)
    #     # fits_data = fits_data.find_all("div") #,class_="data-sheet__row js-equipment-info js-model-item")
    #     # print("4 ",fits_data)
    #     # spc_data = soup.find_all("ul", class_="spec-list")
    #     print("-------------------------##########-------------------------------------------------",fits_data,"--------------------------------------------------------------------------------------")
    #     input()
    #     fits_model = []
    #     # print("\n\n000000000000000000",spc_name.get_text().strip())
    #     for i in range(0,len(fits_data)):
            
            
    #         print(fits_data[i].get_text().strip("").strip("\n").encode("ascii", "ignore").decode("utf-8", "ignore").replace("\n", ""))
    #         compatable = (
    #             spc_data[i]
    #             .get_text()
    #             .strip("")
    #             .strip("\n")
    #             .encode("ascii", "ignore")
    #             .decode("utf-8", "ignore")
    #             .replace("\n", "")
    #         )
    #         print("#########################################################",specifications)
    #         if "quantity" in spc_name[i].get_text.lower():
    #             quantity=spc_data[i].get_text().strip("").strip("\n").encode("ascii", "ignore").decode("utf-8", "ignore").replace("\n", "")
        
    # except:
    #     specifications = {}
    #     input(traceback.format_exc())


    
    data = {}
    data["itemNumber"] = item_no
    data["mpn"] = mpn_no
    data["manufacturer"] = manufacturer
    data["Productname"] = product_name
    data["unitPrice"] = price
    data["packageDetails"] = quantity
    data["UnitOfMeasure"] = unit
    data["description"] = description
    data["additionalSpecs"] = specifications
    data["category"] = category
    data["subCategory"] = sub_category
    data["imageUrl"] = image_url
    data["sourceSite"] = "partstown"
    data["specification"] = specification
    data["part_url_key"] = part_url
    print(data)
    return data
    # return data
final_dict = []
parts = ["TRANSDUCER","TRANSFORMER","CLOSER","DOOR","FRAME","GASKET","GLIDE","GRIP","HANDLE","HARNESS-DOOR","LIFT","PIN","STRUT","TRACK","WIPER","CLAMP","CYLINDER","GLASS","HINGE","MULTIGLASS","PLEXIGLASS","DRAIN","BOLT","COLLAR","COUPLING","DRAIN","FITTING","HOSE","KEY","MAGNET","PLUG","POST","PULL","ROD","SPACER","STRAINER","STUD","TOOL","TRAP","ELBOW","INSERT","INSULATION","MODULE","TAPE","BRACKET-MOTOR"]
# "BALLAST","BATTERY","BLOCK","BOARD","BREAKER",

# "TRANSDUCER","TRANSFORMER","CLOSER","DOOR","FRAME","GASKET","GLIDE","GRIP","HANDLE","HARNESS-DOOR","LIFT","PIN","STRUT","TRACK","WIPER","CLAMP","CYLINDER","GLASS","HINGE","MULTIGLASS","PLEXIGLASS","DRAIN","BOLT","COLLAR","COUPLING","DRAIN","FITTING","HOSE","KEY","MAGNET","PLUG","POST","PULL","ROD","SPACER","STRAINER","STUD","TOOL","TRAP","ELBOW","INSERT","INSULATION","MODULE","TAPE","BRACKET-MOTOR"

# GANESH

# FAN
# FAN BLADE
# GUARD
# MOTOR
# MOTOR ASSY
# RING-MOTOR
# COVER
# DIVIDER
# END
# PAN
# PLATE
# SPLASHGUARD
# TRAY
# RACK
# CAP TUBE
# COIL
# COMPRESSOR
# CONDENSER
# CONDENSING UNIT
# COOLER
# CORE
# DISTRIBUTOR
# DRIER
# EVAPORATOR
# FILTER
# ORIFICE
# PUMP
# RECEIVER
# REGULATOR
# SIGHT GLASS
# SOLENOID
# TANK
# TUBE
# VALVE
# VALVE-BALL
# VALVE-CHECK
# VALVE-SOLENOID
# HARNESS-SHELF
# PRODUCT STOP
# PTM
# SHELF
# BRACKET-MOTOR
# BUMPER
# CAP
# CHANNEL
# CLIP
# CORNER
# DISPLAY
# EXTRUSION
# JOINT
# LEG
# MOULDING
# MOUNT
# RAIL
# RETAINER
# SUPPORT
# TRIM
for j in parts:
    url = "https://www.partstown.com/hoshizaki/parts?q="+j+"&page=0"
    URL = "https://www.partsonestore.com/filterSearch?q="+j+"&page=0"
    req= scraper.get(url)
    print(req)
    soup = BeautifulSoup(req.content, "html.parser")
    #​soup = BeautifulSoup(req.content, "html.parser")
    txt=soup.find_all('div',class_="details")
    print(txt)
    print(len(txt))
    total_part = 0
    try:
        total_part = int(soup.find("span",class_ = "product-tabs-item__count").get_text().strip("(").strip(")"))
    except:
        pass
    print(total_part)
    pages = 0
    if total_part>24:
        pages = math.ceil(total_part/24)
        print(pages)
    else:
        pages = 1


    for i in range(0,pages):
        url = "https://www.partsonestore.com/filterSearch?q="+j+"&page="+str(i)+''
        
        for link in range(len(txt)):
            print(txt[link].find_all('a')[0].get('href'))
            url = "https://www.partsonestore.com/"+txt[link].find_all('a')[0].get('href')
            print("--------------",url)
            final_dict.append(partstown(url,j))
        print("----------------------------------------------------------------------------------------")
        print(final_dict)
        df = pd.DataFrame(final_dict)
        outputfile = 'D:\RAP\partsone_output_'+j+'.xlsx'
        df.to_excel(outputfile,index=False)
        print("done")


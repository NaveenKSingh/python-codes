from bs4 import BeautifulSoup
from zenrows import ZenRowsClient
import math
import time
import random
import openpyxl
import jsonpickle

#function to save html file

def html_file(prod_url,prod_id,part_name):
    # time.sleep(random.uniform(1,10))
    client = ZenRowsClient("9a313fcf5dbdcda815a3f06b8c078c051e15b731")
    # url = "https://www.partstown.com/anthony-international/parts?q=gasket"
    # url = "https://www.partstown.com/anthony-international/ant02-14160-2113"


    # url = "D:\\workfile.html"
    params = {"js_render":"true","antibot":"true"}
    # time.sleep(random.uniform(1,10))
    # prod_url = "https://www.partstown.com//true//true801849"
    response = client.get(prod_url, params=params)
    p = prod_id.split('/')[0]
    content = response.text
    content = content.replace('\u03a9',' ')
    # print(response.text)
    html_filename = 'D:\\Naveen\\anthony\\'+part_name+'_'+p+'.html'
    with open(html_filename, 'w') as f:
        f.write(content)
    print("\nFile Saved\n")

    product_scrapper(html_filename,prod_id,part_name,prod_url)
    

def product_scrapper(file_path,prod_id,part_name,prod_url):
    # file_path = html_filename
    print(part_name)
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            html_content = file.read()
    except:
        with open(file_path, 'r') as file:
            html_content = file.read()
    soup = BeautifulSoup(html_content, 'html.parser')
    

    manufacturer="N/A"
    mpn = "N/A"
    price = "N/A"
    sku = "N/A"
    length = "N/A"
    width = "N/A"
    height = "N/A"
    weight="N/A"
    fits_models = "N/A"
    try:
        manufacturer = soup.find('div',class_="product__cell product__val").find('span',itemprop="manufacturer").text
    except:
        manufacturer = "N/A"
    print(manufacturer)

    try:
        mpn = soup.findAll('div',class_="product__cell product__val")[1].find('span',itemprop="productID").text
    except:
        mpn = "N/A"
    print(mpn)

    try:
        price = soup.find('div',class_="product__cell price-vat price-vat__mobile-only js-product-listPrice").text.strip()
    except:
        price = "N/A"
    print(price)

    try:
        sku = soup.find('span',itemprop="sku").text
    except:
        sku = "N/A"
    print(sku)

    try:
        length = soup.findAll('div',class_="spec-list__value")[0].text.strip('\n').replace("\n",'')
    except:
        length = "N/A"
    print(length)

    try:
        width = soup.findAll('div',class_="spec-list__value")[1].text.strip('\n').replace("\n",'')
    except:
        width = "N/A"
    print(width)

    try:
        height = soup.findAll('div',class_="spec-list__value")[2].text.strip('\n').replace("\n",'')
    except:
        height = "N/A"
    print(height)

    try:
        weight = soup.findAll('div',class_="spec-list__value")[3].text.strip('\n').replace("\n",'')
    except:
        weight = "N/A"
    print(weight)
    lwhw = length+","+width+","+height+","+weight
    lwhw = lwhw.replace("\u00a0"," ")
    print(lwhw)
    # fits_models=soup.find('div',class_="data-sheet data-sheet--vue")
    # print(fits_models)

    try:
        fits_models=soup.find('div',class_="data-sheet data-sheet--vue").findAll('div',class_="data-sheet__row js-equipment-info js-model-item")#.find('span').text
        print("#########",fits_models)
        fit_mod = ''
        c=0
        for mod in fits_models:
            print("*********************",mod.find("div").find('span').text)
            model = mod.find("div").find('span').text

            if c<1:
                fit_mod = model
            else:
                fit_mod = fit_mod+','+model
            c= c+1
        fit_mod = fit_mod.strip(',')
        print("ffiitt",fit_mod)
        fits_models = fit_mod
    except:
        fits_models = "N/A"

    # fits_models=soup.find('div',class_="data-sheet data-sheet--vue").find('div',class_="data-sheet__row js-equipment-info js-model-item")[1]
    # print("#########",fits_models)
    # fits_models = soup.findAll('div',class_="tab-content__data").findAll('div',class_="data-sheet__row js-equipment-info js-model-item").findAll('div',class_="data-sheet__col data-sheet__col--title js-edit-popup-name").find('span')
    # print("------------------------------",fits_models)
    # fits_models = soup.find('div',class_="tab-content__data").findAll('div',class_="data-sheet__col data-sheet__col--title js-edit-popup-name")
    # print("**************************",fits_models)


    # try:
    #     # fits_models = soup.find('div',class_="tab-content__data").findAll('div',class_="data-sheet__col data-sheet__col--title js-edit-popup-name")
    #     fits_models = soup.find('div',class_="tab-content__data").findAll('div',class_="data-sheet__row js-equipment-info js-model-item")

        
    # except:
    #     fits_models = "N/A"
    # input()
    # print(fits_models)
    global failed_cases
    if manufacturer =="N/A" and mpn =="N/A" and sku =="N/A":
        failed_cases.append(prod_id)
        print("failed ----", prod_id)
    else:
        workbook = openpyxl.load_workbook("D:\\Naveen\\anthony\\anthony_international3.xlsx")
        # Select the active sheet
        sheet = workbook.active
        # Data to be written
        max_row = sheet.max_row
        # Column to write the data (e.g., column B)
        colA = 'A'+str(max_row+1)
        colB = 'B'+str(max_row+1)
        colC = 'C'+str(max_row+1)
        colD = 'D'+str(max_row+1)
        colE = 'E'+str(max_row+1)
        colF = 'F'+str(max_row+1)
        colG = 'G'+str(max_row+1)
        colH = 'H'+str(max_row+1)
        colI = 'I'+str(max_row+1)
        print("max_row",colA)
        sheet[colA] = part_name
        sheet[colB] = manufacturer
        sheet[colC] = sku
        sheet[colD] = lwhw
        sheet[colE] = mpn
        sheet[colF] = price
        sheet[colG] = fits_models
        sheet[colH] = prod_url
        sheet[colI] = prod_id
        #colB =
        # Write the data to the specified column
        # for row, value in enumerate(data, start=1):
        # Save the workbook
        workbook.save("D:\\Naveen\\anthony\\anthony_international3.xlsx")

        global products_json
        global product_json_final
        products_json = []
        products_json = [
        {
            "recognized_attributes": {
                "sku": sku,
                "url": prod_url,
                "title": part_name,
                "product-id": prod_id,
                "main-description": lwhw,
                "oem-product-id": "N/A",
                "compliant-with": fits_models,
                "manufacturer": manufacturer,
            },
            "all_extracted_data": [
                {
                    "extraction_priority": 1,
                    "details": lwhw,
                    "media_type": "text",
                    "format_type": "table",
                    "content": "N/A"
                },
                {
                    "extraction_priority": 2,
                    "Similar Models": fits_models,
                    "media_type": "text",
                    "format_type": "table",
                    "content": "N/A"
                },
                {
                    "extraction_priority": 3,
                    "Dimensions":lwhw,
                    "media_type": "text",
                    "format_type": "table",
                    "content": "N/A"
                }
               ]
               }
              ]
        product_json_final.append(products_json)
        

    


#scraping prod id from product landing page

def scrape_data(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            html_content = file.read()
    except:
        with open(file_path, 'r') as file:
            html_content = file.read()
    soup = BeautifulSoup(html_content, 'html.parser')
    # Your scraping logic here
    # print(soup)
    scraped_data = []
    
    data_elements = soup.find_all('div', class_='product__list--wrapper')
    data_elements = soup.find('ul', class_='product__listing product__list js-parts-listing-holder')
    data_elements = soup.find('ul', class_ ='product-info')
    data_elements = soup.find_all('strong')
    data_elements = soup.find_all('strong')
    # print("@@@@@@@@@@@@@@@@",data_elements)
    
    pro_id = []
    
    for el in data_elements:
        # print("--------------------",el)
        if 'ANT' in str(el):
            pro_id.append(el)
            print(pro_id)

    total_items = soup.find('div',class_='product-pagination__items').text.split("of")[1].split(" ")[1].rstrip()
    total_pages = math.ceil(int(total_items)/24)
    print(total_items, total_pages)

    for i in range(1,total_pages):
        product_page_url = "https://www.partstown.com/anthony-international/parts?q="+part_name+"&page="+i
        params = {"js_render":"true","antibot":"true","premium_proxy":"true"}
        response = client.get(product_page_url, params=params)

        content = response.text
            # print(response.text)
        html_filename = 'D:\\Naveen\\anthony\\workfile_firstpage_'+part_name+'.html'
        with open(html_filename, 'w') as f:
            f.write(content)
        print("\nFile Saved\n")

        scrape_data(html_filename)



    

    final_prod = []
    for pid in pro_id:
        b = str(pid).split("</span>")[-1].split('<')[0].strip()
        final_prod.append(b)
        prod_url = 'https://www.partstown.com/anthony-international/'+b
        # html_file(prod_url,b)
    print(final_prod)








if __name__ == "__main__":
    
    # file_path = "D:\\workfile.html"  
    # product_scrapper("D:\\Naveen\\anthony\\workfile_ANT02-14160-2113.html") 
    # product_scrapper("D:\\Naveen\\anthony\\workfile_1234567890.html") 
    client = ZenRowsClient("9a313fcf5dbdcda815a3f06b8c078c051e15b731")

    # scraped_data = scrape_data(file_path)
    # parts = ["CURTAIN","CUTTER","FENCE","HOLDER","MIRROR","DAMPER","GRILL","HONEYCOMB","LOUVER","SWEEP","BALLAST","BATTERY","BLOCK","BOARD","BREAKER","CABLE","CAPACITOR","CONNECTOR","CONTACTOR","CONTROLLER","CORD","DIFFUSER","DIODE","DRIVE","DRIVER","ENCLOSURE","FIXTURE","FUSE","GAUGE","HARNESS","HEATER","INDICATOR","JUMPER","LAMP","LED","LEVELER","MONITOR","POWER SUPPLY","RECEPTACLE","RELAY","SENSOR","SHIELD","SOCKET","STARTER","SWITCH","THERMOMETER","THERMOSTAT","TIME CLOCK","TRANSDUCER","TRANSFORMER","CLOSER","DOOR","FRAME","GASKET","GLIDE","GRIP","HANDLE","HARNESS-DOOR","LIFT","PIN","STRUT","TRACK","WIPER","CLAMP","CYLINDER","GLASS","HINGE","MULTIGLASS","PLEXIGLASS","DRAIN","BOLT","COLLAR","COUPLING","DRAIN","FITTING","HOSE","KEY","MAGNET"***,"PLUG","POST","PULL","ROD","SPACER","STRAINER","STUD","TOOL","TRAP","ELBOW","INSERT","INSULATION","MODULE","TAPE","BRACKET-MOTOR","FAN BLADE","GUARD","MOTOR ASSY","RING-MOTOR","COVER","DIVIDER","END",#########"PAN","PLATE","SPLASHGUARD","TRAY","RACK","CAP TUBE","COIL","COMPRESSOR","CONDENSER","CONDENSING UNIT","COOLER","CORE","DISTRIBUTOR","DRIER","EVAPORATOR","FILTER","ORIFICE","PUMP","RECEIVER","REGULATOR","SIGHT GLASS","SOLENOID","TANK","TUBE","VALVE","VALVE-BALL","VALVE-CHECK","VALVE-SOLENOID","HARNESS-SHELF","PRODUCT STOP","PTM","SHELF","BRACKET-MOTOR","BUMPER","CAP","CHANNEL","CLIP","CORNER","DISPLAY","EXTRUSION","JOINT","LEG","MOULDING","MOUNT","RAIL","RETAINER","SUPPORT","TRIM"]
    #"gasket",
    # completed = ["BALLAST","CURTAIN","CUTTER","FENCE","HOLDER","MIRROR","DAMPER","GRILL","HONEYCOMB","LOUVER","SWEEP","BATTERY","BLOCK","BOARD","BREAKER","CABLE","CAPACITOR","CONNECTOR","CONTACTOR","CONTROLLER","CORD"]
    # parts = ["BALLAST","CURTAIN","CUTTER","FENCE","HOLDER","MIRROR","DAMPER","GRILL","HONEYCOMB","LOUVER","SWEEP","BATTERY","BLOCK","BOARD","BREAKER","CABLE","CAPACITOR","CONNECTOR","CONTACTOR","CONTROLLER","CORD","DIFFUSER","DIODE","DRIVE","DRIVER","ENCLOSURE","FIXTURE","FUSE","GAUGE","HARNESS","HEATER","INDICATOR","JUMPER","LAMP","LED","LEVELER","MONITOR","POWER SUPPLY","RECEPTACLE","RELAY","SENSOR","SHIELD","SOCKET","STARTER","SWITCH","THERMOMETER","THERMOSTAT"]
        # url = "D:\\workfile.html"
        #"DOOR","FRAME","GASKET",
    parts = ["GLIDE","GRIP","HANDLE","HARNESS-DOOR","LIFT","PIN","STRUT","TRACK","WIPER","CLAMP","CYLINDER","GLASS","HINGE","MULTIGLASS","PLEXIGLASS","DRAIN","BOLT","COLLAR","COUPLING","DRAIN","FITTING","HOSE","KEY","MAGNET"]
    for part_name in parts:
        print(part_name)
        total_pages=1
        page_no = 0
    
        pro_id = []
        final_prod = []
        failed_cases = []
        global product_json_final
        product_json_final = []

        def first_page():
                product_page_url = "https://www.partstown.com/anthony-international/parts?q="+part_name+"&page=0"
                params = {"js_render":"true","antibot":"true","premium_proxy":"true"}
                response = client.get(product_page_url, params=params)

                content = response.text
                    # print(response.text)
                html_filename = 'D:\\Naveen\\anthony\\workfile_firstpage_'+part_name+'.html'
                with open(html_filename, 'w') as f:
                    f.write(content)
                print("\nFile Saved\n")

                file_path = html_filename

                try:
                    with open(file_path, 'r', encoding='utf-8') as file:
                        html_content = file.read()
                except:
                    with open(file_path, 'r') as file:
                        html_content = file.read()
                soup = BeautifulSoup(html_content, 'html.parser')
                global total_items
                try:
                    total_items = soup.find('div',class_='product-pagination__items').text.split("of")[1].split(" ")[1].rstrip()
                except:
                    total_items =0
                    # return
        for i in range(0,1):            
            try:
                first_page()
                
            except:
                try:
                    first_page()
                except:
                    first_page()
            global total_items
            total_pages = math.ceil(int(total_items)/24)
            print(total_items, total_pages)

        # def all_productlist_scrapper():
        if(total_pages>0):
            for pages in range(0,total_pages):


                product_page_url = "https://www.partstown.com/anthony-international/parts?q="+part_name+"&page="+str(pages)
                params = {"js_render":"true","antibot":"true","premium_proxy":"true"}
                response = client.get(product_page_url, params=params)

                content = response.text
                content = content.replace('\u03a9',' ')
                    # print(response.text)
                html_filename = 'D:\\Naveen\\anthony\\workfile_firstpage_'+part_name+str(pages)+'.html'
                with open(html_filename, 'w') as f:
                    f.write(content)
                print("\nFile Saved\n")

                file_path = html_filename

                try:
                    with open(file_path, 'r', encoding='utf-8') as file:
                        html_content = file.read()
                except:
                    with open(file_path, 'r') as file:
                        html_content = file.read()
                soup = BeautifulSoup(html_content, 'html.parser')





                # scrape_data(html_filename)
                data_elements = soup.find_all('div', class_='product__list--wrapper')
                data_elements = soup.find('ul', class_='product__listing product__list js-parts-listing-holder')
                data_elements = soup.find('ul', class_ ='product-info')
                data_elements = soup.find_all('strong')
                data_elements = soup.find_all('strong')
                print("@@@@@@@@@@@@@@@@",data_elements)
                
                pro_id = []
                
                for el in data_elements:
                    # print("--------------------",el)
                    if 'ANT' in str(el):
                        pro_id.append(el)
                        print(pro_id)
                print(len(pro_id))

                # final_prod = []
                for pid in pro_id:
                    b = str(pid).split("</span>")[-1].split('<')[0].strip()
                    final_prod.append(b)
                    
                    # html_file(prod_url,b)
                print(final_prod,len(final_prod))

            for each_product in final_prod:
            # for i in range(0,5):
                # each_product = final_prod[i]
                prod_url = 'https://www.partstown.com/anthony-international/'+each_product
                html_file(prod_url,each_product,part_name)
## Failed Cases           
            print(failed_cases)

            
            
            failed_cases_list = []
            def failed():
                global failed_cases
                failed_cases_list = failed_cases
                failed_cases = []
                print(failed_cases_list)
                length = len(failed_cases_list)
                print(length)
                global count
                if length < 1 or count>4:
                    return
                for each_failed_product in failed_cases_list:
                    prod_url = 'https://www.partstown.com/anthony-international/'+each_failed_product
                    print(prod_url)
                    html_file(prod_url,each_failed_product,part_name)
                # global count
                count = count+1
                print("-----------",failed_cases)
                failed()
            global count
            count = 0
            
            failed()

            dataj={
            "portal": "partstown.com",
            "category": part_name,
            "products":product_json_final
            }
            print(dataj)
            json_f='partstown_anthony_'+part_name+'_'+'_04_jul_2023_set1.json'
            # json_data = json.dump(dataj)
            file_path = 'D:\\Naveen\\anthony\\json\\'+json_f
            # with open(file_path, "r") as file:
            #     json_data1 = json.load(file)
            # json_data1.append(dataj)
            with open(file_path, "w") as file:
                #json.dump(dataj,file,indent=4)
                s_data = jsonpickle.encode(dataj)
                file.write(s_data)
            print("Done............")
        
        else:
            
            product_page_url = "https://www.partstown.com/anthony-international/parts?q="+part_name+"&page=0"
            params = {"js_render":"true","antibot":"true","premium_proxy":"true"}
            response = client.get(product_page_url, params=params)

            content = response.text
                # print(response.text)
            html_filename = 'D:\\Naveen\\anthony\\workfile_firstpage_'+part_name+'.html'
            with open(html_filename, 'w') as f:
                f.write(content)
            print("\nFile Saved\n")

            file_path = html_filename

            try:
                with open(file_path, 'r', encoding='utf-8') as file:
                    html_content = file.read()
            except:
                with open(file_path, 'r') as file:
                    html_content = file.read()
            soup = BeautifulSoup(html_content, 'html.parser')





            # scrape_data(html_filename)
            data_elements = soup.find_all('div', class_='product__list--wrapper')
            data_elements = soup.find('ul', class_='product__listing product__list js-parts-listing-holder')
            data_elements = soup.find('ul', class_ ='product-info')
            data_elements = soup.find_all('strong')
            data_elements = soup.find_all('strong')
            print("@@@@@@@@@@@@@@@@",data_elements)
            
            pro_id = []
            
            for el in data_elements:
                # print("--------------------",el)
                if 'ANT' in str(el):
                    pro_id.append(el)
                    print(pro_id)
            print(len(pro_id))

            # final_prod = []
            for pid in pro_id:
                b = str(pid).split("</span>")[-1].split('<')[0].strip()
                final_prod.append(b)
                
                # html_file(prod_url,b)
            print(final_prod,len(final_prod))
        

            for each_product in final_prod:
            # for i in range(0,5):
                # each_product = final_prod[i]
                prod_url = 'https://www.partstown.com/anthony-international/'+each_product
                html_file(prod_url,each_product,part_name)
    ## Failed Cases           
            print(failed_cases)

            
            
            failed_cases_list = []
            def failed(c):
                global failed_cases
                failed_cases_list = failed_cases
                failed_cases = []
                print(failed_cases_list)
                length = len(failed_cases_list)
                print(length)
                if length < 1 or c>4:
                    return
                for each_failed_product in failed_cases_list:
                    prod_url = 'https://www.partstown.com/anthony-international/'+each_failed_product
                    print(prod_url)
                    html_file(prod_url,each_failed_product,part_name)
                # global c
                c = c+1
                print("-----------",failed_cases)
                failed(c)

            # global c
            c=0
                                            
            failed(c)

            dataj={
            "portal": "partstown.com",
            "category": part_name,
            "products":product_json_final
            }
            print(dataj)
            json_f='partstown_anthony_'+part_name+'_'+'_04_jul_2023_set1.json'
            # json_data = json.dump(dataj)
            file_path = 'D:\\Naveen\\anthony\\json\\'+json_f
            # with open(file_path, "r") as file:
            #     json_data1 = json.load(file)
            # json_data1.append(dataj)
            with open(file_path, "w") as file:
                #json.dump(dataj,file,indent=4)
                s_data = jsonpickle.encode(dataj)
                file.write(s_data)
            print("Done............")

            # except:
            #     print("NO PARTS AVAILABLE")
    



        
        




    
                    


            



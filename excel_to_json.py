import pandas as pd
import json
# Read Excel file into a pandas DataFrame
df = pd.read_excel('D:\RAP\Hussman\partsone_final_updated_huss - Copy.xlsx')
# Specify the column name to be used as keys in the JSON
key_column = 'Category'
# Convert DataFrame to JSON
json_data = {}
for _, row in df.iterrows():
    key = row[key_column]
    json_data[key] = row.to_dict()
# Convert the JSON data to a string
json_string = json.dumps(json_data, indent=4)
# Save the JSON data to a file
with open('output.json', 'w') as f:
    f.write(json_string)
# Alternatively, you can print the JSON data
print(json_string)


dataj={
    "portal": "partfe.com",
    "category": "bracket-motor",
    "products": [
        {
            "recognized_attributes": {
                "sku": Asap,
                "url": url,
                "title": product_name,
                "product-id": oem_part_number,
                "main-description": product_description,
                "oem-product-id": oem_part_number,
                "compliant-with": "N/A",
                "manufacturer": manufacturer_name,
                "Image URL": "N/A"
            },
            "all_extracted_data": [
                {
                    "extraction_priority": 1,
                    "details": product_description,
                    "media_type": "text",
                    "format_type": "table",
                    "content": "N/A"
                },
                {
                    "extraction_priority": 2,
                    "Similar Models": similar_models,
                    "media_type": "text",
                    "format_type": "table",
                    "content": "N/A"
                },
                {
                    "extraction_priority": 3,
                    "Dimensions":dimensions,
                    "media_type": "text",
                    "format_type": "table",
                    "content": "N/A"
                }
            ]
        }
    ]
}
    json_f='partsfe_bracket_motor_26_may_2023_set1.json'
    # file_path = "path/to/file.json"
    with open(json_f, "w") as file:
        json.dump(dataj, file , indent=4)